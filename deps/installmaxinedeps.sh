cd ..
git clone https://github.com/beehive-lab/Maxine-Graal.git graal

cd /tmp

export ARCH=amd64 # or arm64

wget http://snapshot.debian.org/archive/debian/20170522T033408Z/pool/main/o/openjdk-7/openjdk-7-jdk_7u131-2.6.9-3_$ARCH.deb
wget http://snapshot.debian.org/archive/debian/20170522T033408Z/pool/main/o/openjdk-7/openjdk-7-jre_7u131-2.6.9-3_$ARCH.deb
wget http://snapshot.debian.org/archive/debian/20170522T033408Z/pool/main/o/openjdk-7/openjdk-7-jre-headless_7u131-2.6.9-3_$ARCH.deb
wget http://snapshot.debian.org/archive/debian/20170522T033408Z/pool/main/o/openjdk-7/openjdk-7-dbg_7u131-2.6.9-3_$ARCH.deb

wget http://ftp.uk.debian.org/debian/pool/main/libj/libjpeg-turbo/libjpeg62-turbo_1.5.1-2_$ARCH.deb
sudo dpkg -i openjdk-7-jdk_7u131-2.6.9-3_$ARCH.deb openjdk-7-jre_7u131-2.6.9-3_$ARCH.deb openjdk-7-jre-headless_7u131-2.6.9-3_$ARCH.deb openjdk-7-dbg_7u131-2.6.9-3_$ARCH.deb libjpeg62-turbo_1.5.1-2_$ARCH.deb
sudo apt-get install -f

