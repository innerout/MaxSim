wget https://github.com/google/protobuf/releases/download/v2.6.1/protobuf-2.6.1.tar.gz
tar xf protobuf-2.6.1.tar.gz
cd protobuf-2.6.1
LDFLAGS="-static-libgcc -static-libstdc++" ./configure --disable-shared --prefix=/usr/local/protobuf-2.6.1
cd src
make protoc
make install
